# Plom ID Reader data
This repository contains the tensorflow model and scikit-learn models used by Plom for reading digits from student IDs.

* The script to build and train the scikit-learn model is in the Plom source code.
* From Plom 0.7.5 onward (Autumn 2021), scikit-learn model includes the scikit-learn version in the filename.

## Maintenance

* Copy the latest `trainRandomForestModel.py` from the stable `main` branch of Plom to the base directory of this repo.
* pip install the version of `scikit-learn` that you wish to build for.
* `python3 trainRandomForestModel.py`  (should take a couple mins).
* move the new `.gz` from `model_cache` and check it into `plomBuzzword` directory.
* Push to the gitlab.com repo.
